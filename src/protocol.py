import json

from collections import namedtuple


class ProtocolCommands:
    GET = 0
    SEND = 1


Package = namedtuple('ProtocolCommand', ['name', 'command', 'value'])

# example command
cmd_get = Package(name='Bot', command=ProtocolCommands.SEND, value=0)


def package_to_bytes(package: Package):
    return json.dumps(package._asdict())  # TODO -- json to bytes


def bytes_to_package(bytes_stream):
    my_json = bytes_stream.decode('utf8').replace("'", '"')
    data = json.loads(my_json)
    s = json.dumps(data, indent=4, sort_keys=True)
    return s  # TODO -- json to namedtuple
